package com.zwj.quba.ui;

/**
 * Created by Administrator on 15-7-6.
 */
public class QB_Url {
    //  详情数据
    public static final String details_URL = "http://app.quba360.com/product/info";
    //顶部购物车内容数量
    public static final String SHOPPING_URL = "http://app.quba360.com/shoppingcart/count";
    //评论数据:
    public static final String COMMENT_URL = "http: //app.quba360.com/product/comments";
    //顶部ViewPager广告栏:
    public static final String TOP_ViewPager_URL = "http://app.quba360.com/product/getBanners";
    //横向滚动栏数据：
    public static final String ROLL_URL = "http://app.quba360.com/product/activities";
    //横向滚动详情（广告栏）
    public static final String AD_URL = "http://app.quba360.com/product/activityInfo";
    //横向滚动详情
    public static final String CROSS_URL = "http://app.quba360.com/product/activityProducts";
    //商品分类(首页最下边的数据)
    public static final String GOOD_URL = "http://app.quba360.com/product/getRecommendProducts";
    //搜索
    public static final String SEARCH_URL = "http://app.quba360.com/product/getProductList";
    //商品分类(四个一样)
    public static final String COMPOSITE_URL = "http://app.quba360.com/product/getProductList";
    //立即购买—会动的图片（商家信息）
    public static final String MOVEIMAGE_URL = "http://app.quba360.com/user/infoUser";
    //立即购买—会动的图片（商家品信息信息）
    public static final String SHOP_MESSAGE_URL = "http://app.quba360.com/product/sellerStoreOnlineProducts";
    //购物车
    public static final String SHOP_CAR_URL = "http://app.quba360.com/shoppingcart/get";
    //物品收藏
    public static final String COLLECT_URL = "http://app.quba360.com/product/favorite";

}
